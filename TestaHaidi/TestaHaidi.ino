byte directionPin = 2;
byte stepPin = 3;
int numberOfSteps = 10;
int pulseWidthMicros = 20;  // microseconds
int millisbetweenSteps = 100; 

enum Direzione {
  DESTRA,
  SINISTRA
};

void setup() {
  Serial.begin(115200);
  // put your setup code here, to run once:
  pinMode(directionPin, OUTPUT);
  pinMode(stepPin, OUTPUT);
}



void rotate_head(Direzione d){
  byte livello = HIGH;
  if(d == SINISTRA)
    livello = LOW;
    
  digitalWrite(directionPin, livello);
  for(int n = 0; n < numberOfSteps; n++) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(pulseWidthMicros); // this line is probably unnecessary
    digitalWrite(stepPin, LOW);
    
    delay(millisbetweenSteps);
  }
}
  

void loop() {
  if(Serial.available()!=0) {
    char cmd = Serial.read();
    Serial.print(cmd);
    switch(cmd) {
      case 'd': 
      rotate_head(DESTRA);
      break;
      case 's': 
      rotate_head(SINISTRA);
      break;
      default: 
      pinMode(directionPin, OUTPUT);
      pinMode(stepPin, OUTPUT);
      break;
      }
    }
}
