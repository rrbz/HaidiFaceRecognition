#!/usr/bin/env python3
import face_recognition
import pickle

face_encodings = { }

sasha_image = face_recognition.load_image_file("sasha.jpg")
face_encodings["Sasha"] = face_recognition.face_encodings(sasha_image)[0]

luca_image = face_recognition.load_image_file("luca.jpg")
face_encodings["Sua Eccellenza"] = face_recognition.face_encodings(luca_image)[0]

sanin_image = face_recognition.load_image_file("sanin.jpg")
face_encodings["Prof. Sanin"] = face_recognition.face_encodings(sanin_image)[0]

#fontana_image = face_recognition.load_image_file("fontana.jpg")
#face_encodings["Dr. Fontana"] = face_recognition.face_encodings(fontana_image)[0]

#bertotto_image = face_recognition.load_image_file("bertotto.jpg")
#face_encodings["Prof. Bertotto"] = face_recognition.face_encodings(bertotto_image)[0]

#mattia_image = face_recognition.load_image_file("mattia.jpg")
#face_encodings["Mattia"] = face_recognition.face_encodings(mattia_image)[0]

#samuele_image = face_recognition.load_image_file("samuele.jpg")
#face_encodings["Samuele"] = face_recognition.face_encodings(samuele_image)[0]

##patrick_image = face_recognition.load_image_file("patrick.jpg")
#face_encodings["Sua altezza Patrick"] = face_recognition.face_encodings(patrick_image)[0]

#alex_image = face_recognition.load_image_file("alex.jpg")
#face_encodings["Alex"] = face_recognition.face_encodings(alex_image)[0]

#paolo_image = face_recognition.load_image_file("paolo.jpg")
#face_encodings["Paolo"] = face_recognition.face_encodings(paolo_image)[0]

with open('dataset_faces.dat', 'wb') as f:
    pickle.dump(face_encodings, f)