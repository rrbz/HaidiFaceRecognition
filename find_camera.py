import numpy as np
import cv2


cams_test = 2
for i in range(0, cams_test):
    cap = cv2.VideoCapture(i)
    test, frame = cap.read()
    
    #Get resolution
    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
    

    #print("camera id : "+str(i)+" /// active: "+str(test)+" /// resolution: "+str(width), str(height)+"\n")
    if test is False:
        #print("id cam: 0 is active")
        cam = 0
    else:
        cam = 1
        #print("id cam: 1 is active")

#print("correct camera id: "+cam)
if cam == 1:
    print("External camera connected succesfully with id : ",cam)
else:
    print("Default Mac camera with id : ",cam)
