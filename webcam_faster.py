#!/usr/bin/env python3
import face_recognition
import cv2
import pickle
import numpy as np
from serial import Serial
import serial.tools.list_ports as tty

# Head articulation constants
SKETCH_NAME = "HeadArticulation"
SKETCH_VER = "1.0"
ROT_LEFT = 'l'
ROT_LEFT_THR = 80 # upper dead band
ROT_RIGHT = 'r'
ROT_RIGHT_THR = -80 # lower dead band

# Find serial port with HAIDI head articulation script
head_serial = None
for port in tty.comports():
    try:
        device = port.device
        serial = Serial(device, 115200, timeout=3)
        sketch_info = serial.readline().decode("ascii", "ignore")
        if SKETCH_NAME + ' ' + SKETCH_VER in sketch_info:
            print("Found Arduino at ", device)
            head_serial = serial
            sketch_cmds = serial.readline().decode("ascii", "ignore")
            print(sketch_cmds)
    except OSError:
        pass
if head_serial is None:
    print("No serial port found.")

def setHead(articulation):
    print("Head:", articulation)
    if head_serial is None:
        return
    try:
        head_serial.write(articulation.encode('ascii'))
    except:
        pass

# This is a demo of running face recognition on live video from your webcam. It's a little more complicated than the
# other example, but it includes some basic performance tweaks to make things run a lot faster:
#   1. Process each video frame at 1/4 resolution (though still display it at full resolution)
#   2. Only detect faces in every other frame of video.

# PLEASE NOTE: This example requires OpenCV (the `cv2` library) to be installed only to read from your webcam.
# OpenCV is *not* required to use the face_recognition library. It's only requiredfac if you want to run this
# specific demo. If you have trouble installing it, try any of the other demos that don't require it instead.

# Select the external camera by default, if connected.
cams_test = 2
for i in range(0, cams_test):
    cap = cv2.VideoCapture(i)
    test, frame = cap.read()    

    if test is False:
        cam = 0
    else:
        cam = 1

if cam == 1:
    print("External camera connected succesfully with id : ",cam)
else:
    print("Default Mac camera with id : ",cam)

#Set the correct camera found before
video_capture = cv2.VideoCapture(cam)

# Create arrays of known face encodings and their names
known_face_encodings = [ ]
known_face_names = [ ]

try:
    # Load face encodings
    with open('dataset_faces.dat', 'rb') as f:
        face_encodings = pickle.load(f)

        # Grab the list of names and the list of encodings
        face_names = list(face_encodings.keys())
        face_encodings = np.array(list(face_encodings.values()))
        known_face_encodings.extend(face_encodings)
        known_face_names.extend(face_names)
        print("Dataset faces found. Loaded", face_names, "faces.")

except FileNotFoundError:
    print("Dataset faces not found. Continue using default faces.")
    
    # Load a sample picture and learn how to recognize it.
    obama_image = face_recognition.load_image_file("obama.jpg")
    obama_face_encoding = face_recognition.face_encodings(obama_image)[0]

    # Load a second sample picture and learn how to recognize it.
    biden_image = face_recognition.load_image_file("biden.jpg")
    biden_face_encoding = face_recognition.face_encodings(biden_image)[0]

    sasha_image = face_recognition.load_image_file("sasha.jpg")
    sasha_face_encoding = face_recognition.face_encodings(sasha_image)[0]

    luca_image = face_recognition.load_image_file("luca.jpg")
    luca_face_encoding = face_recognition.face_encodings(luca_image)[0]

    sanin_image = face_recognition.load_image_file("sanin.jpg")
    sanin_face_encoding = face_recognition.face_encodings(sanin_image)[0]

    # Create arrays of known face encodings and their names
    known_face_encodings.extend([
        obama_face_encoding,
        biden_face_encoding,
        sasha_face_encoding,
        luca_face_encoding,
        sanin_face_encoding
    ])
    known_face_names.extend([
        "Barack Obama",
        "Joe Biden",
        "Sasha",
        "Luca",
        "Prof. Sanin"
    ])

old_center = None
def follow(name, left, right):
    global old_center
    center = None
    if name in known_face_names:
        #print("left:",left,"right:",right)
        center = (left + right) / 2
        #print("Center:", center, "OldCenter:", old_center)
        if old_center is not None:
            diff = int(center - 640)
            if diff != 0:
                print("dRot:", diff)
                rot = ROT_LEFT if diff > ROT_LEFT_THR else \
                    ROT_RIGHT if diff < ROT_RIGHT_THR else None
                if rot is not None:
                    setHead(rot)
    if center is not None:
        old_center = center

# Initialize some variables
face_locations = []
face_encodings = []
face_names = []
process_this_frame = True

while True:
    # Grab a single frame of video
    ret, frame = video_capture.read()

    # Resize frame of video to 1/4 size for faster face recognition processing
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_small_frame = small_frame[:, :, ::-1]

    # Only process every other frame of video to save time
    if process_this_frame:
        # Find all the faces and face encodings in the current frame of video
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
            name = "Sconosciuto"

            # # If a match was found in known_face_encodings, just use the first one.
            # if True in matches:
            #     first_match_index = matches.index(True)
            #     name = known_face_names[first_match_index]

            # Or instead, use the known face with the smallest distance to the new face
            face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_face_names[best_match_index]

            face_names.append(name)

    process_this_frame = not process_this_frame


    # Display the results
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Scale back up face locations since the frame we detected in was scaled to 1/4 size
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # Draw a box around the face
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        follow(name, left, right)

        # Draw a label with a name below the face
        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

    # Display the resulting image
    cv2.imshow('Video', frame)

    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release handle to the webcam
video_capture.release()
cv2.destroyAllWindows()
