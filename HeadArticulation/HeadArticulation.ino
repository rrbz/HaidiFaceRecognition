#define SKETCH_NAME "HeadArticulation"
#define SKETCH_VERSION "1.0"
#define SKETCH_CMDS "Cmds: (i)nfo, (l)eft, (r)ight"

#define echo(x) Serial.print(x)
#define echonl(x) Serial.println(x)
#define debug(x) //Serial.print(x)
#define debugnl(x) //Serial.println(x)

enum StepperMotor {
  DIR_PIN = 2,
  STEP_PIN = 3,
  NUM_STEPS = 1, // Less rotation
  PULSE_WIDTH_US = 20,
  STEP_PAUSE_MS = 20,
};

enum Rotation {
  LEFT = HIGH,
  RIGHT = LOW
};

enum LEDs {
  LED_PIN_1 = 4,
  LED_PIN_2 = 7,
  LED_PIN_3 = 8,
  LED_PIN_4 = 12,
  LED_PIN_5 = 13,
};

enum Mood {
  LED_TEST,
  LED_OFF,
  SAD,
  NORMAL,
  ANGRY,
};

void getInfo() {
  echo(SKETCH_NAME);
  echo(' ');
  echonl(SKETCH_VERSION);
  echonl(SKETCH_CMDS);
}

void setHead(Rotation rot) {
  byte out = rot;
  digitalWrite(DIR_PIN, out);
  debug("digitalWrite("); debug(DIR_PIN); debug(','); debug(out); debugnl(");");
  for(int n = 0; n < NUM_STEPS; n++) {
    digitalWrite(STEP_PIN, HIGH);
    debug("digitalWrite("); debug(STEP_PIN); debug(','); debug(HIGH); debugnl(");");
    delayMicroseconds(PULSE_WIDTH_US);
    digitalWrite(STEP_PIN, LOW);
    debug("digitalWrite("); debug(STEP_PIN); debug(','); debug(LOW); debugnl(");");
    delay(STEP_PAUSE_MS);
  }
}

void setHead(Mood newMood) {
  static const byte LED_CHANNELS = 5;
  static const byte MOOD_TBL_SIZE = LED_CHANNELS + 1;
  static const byte leds[LED_CHANNELS] {
    LED_PIN_1, LED_PIN_2, LED_PIN_3, LED_PIN_4, LED_PIN_5
  };
  static const byte emotions[ ][MOOD_TBL_SIZE] {
    { LED_TEST, HIGH, HIGH, HIGH, HIGH, HIGH },
    { LED_OFF,  LOW,  LOW,  LOW,  LOW,  LOW },
    { SAD,      HIGH, HIGH, LOW,  HIGH, LOW },
    { NORMAL,   HIGH, LOW,  HIGH, HIGH, LOW },
    { ANGRY,    HIGH, LOW,  HIGH, LOW,  HIGH },
  };
  static bool isInitialized = false;
  if(!isInitialized) {
    for(byte ledPin : leds) {
      pinMode(ledPin, OUTPUT);
    }
    isInitialized = true;
  }
  for(byte * emotion : emotions) {
    const byte mood = emotion[0];
    if(mood == newMood) {
      Serial.println(emotion[0], DEC);
      for(byte i = 0; i < LED_CHANNELS; i++) {
        const byte ledPin = leds[i];
        const byte ledOut = emotion[MOOD_TBL_SIZE - LED_CHANNELS + i];
        digitalWrite(ledPin, ledOut);
        debug("digitalWrite("); debug(ledPin); debug(','); debug(ledOut); debugnl(");");
      }
    }
  }
}

void setup() {
  pinMode(DIR_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  Serial.begin(115200);
  while(!Serial);
  while(Serial.available()) {
    byte flush = Serial.read();
  }
  getInfo();
  setHead(LED_TEST);
  setHead(LEFT);
  setHead(RIGHT);
  setHead(NORMAL);
}

void loop() {
  if(Serial.available() > 0) {
    char cmd = Serial.read();
    echo("Cmd: "); echonl(cmd);
    switch(cmd) {
      case 'l': 
      setHead(LEFT);
      break;
      case 'r': 
      setHead(RIGHT);
      break;
      case 'i': 
      getInfo();
      break;
      default:
      pinMode(DIR_PIN, OUTPUT);
      pinMode(STEP_PIN, OUTPUT);
      break;
    }
    while(Serial.available()) {
      byte flush = Serial.read();
    }
  }
}
